package com.aeq.controller;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import com.aeq.model.Address;
import com.aeq.model.Workers;

public class WorkersMain {
	public static void main(String[] args) {
		//@SuppressWarnings("deprecation")
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session session = sf.openSession();
		Transaction tx = null;
		try {
			tx=session.beginTransaction();
			Address a=new Address();
			a.setDoorNo(157);
			a.setStreetName("Kurinji nager");
			a.setCity("Banglore");
			Workers w = new Workers();
			w.setEmpName("Princya");
			w.setEmpAddress(a);
			Long id=(Long) session.save(w);
			Workers workers=(Workers) session.get(Workers.class, id);
			Address address= workers.getEmpAddress();
			System.out.println(workers.getEmpID()+" "+workers.getEmpName()+" "+address.getAddressId()+" "+address.getDoorNo()+" "+address.getStreetName()+" "+address.getCity());
			//session.save(w);
			tx.commit();
		} catch (HibernateException he) {
			tx.rollback();
		}finally {
			session.close();
		}
	}
}
