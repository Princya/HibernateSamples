package com.aeq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity (name = "ADDRESS")
public class Address {
	@Id
	@Column (name="ADDRESS_ID")
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long addressId;
	@Column (name="DOOR_NO")
	private int doorNo;
	@Column (name= "STREET_NAME")
	private String streetName;
	@Column (name="CITY")
	private String city;
	
	public long getAddressId() {
		return addressId;
	}
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}
	public int getDoorNo() {
		return doorNo;
	}
	public void setDoorNo(int doorNo) {
		this.doorNo = doorNo;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	}
