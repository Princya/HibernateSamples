package com.aeq.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity (name="WORKERS")
public class Workers {
	private long empID;
	private String empName;
	private Address empAddress;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "EMP_ID")
	public long getEmpID() {
		return empID;
	}
	public void setEmpID(long empID) {
		this.empID = empID;
	}
	@Column (name = "EMP_NAME")
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="ADDRESS_ID")
	public Address getEmpAddress() {
		return empAddress;
	}
	public void setEmpAddress(Address empAddress) {
		this.empAddress = empAddress;
	}
}
