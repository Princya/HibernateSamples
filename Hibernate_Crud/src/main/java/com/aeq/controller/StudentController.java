package com.aeq.controller;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.aeq.model.Address;
import com.aeq.model.Phone;
import com.aeq.model.Student;
import com.aeq.model.Student_Marks;

public class StudentController {
	public static void main(String[] args) {
		SessionFactory sf=new Configuration().configure().buildSessionFactory();
		Session session=sf.openSession();
		Transaction transaction=null;
		try{
			transaction=session.beginTransaction();
		//First record	
			Address address=new Address();
			address.setStreet("Gandhi nagar");
			address.setCity("Chennai");
			address.setState("TN");
			address.setZipcode(6348743);
			Student_Marks studentMarks=new Student_Marks();
			studentMarks.setTamil(55);
			studentMarks.setEnglish(67);
			studentMarks.setMaths(95);
			studentMarks.setScience(77);
			studentMarks.setSocial(67);
			studentMarks.setTotal();
			studentMarks.setResult();
			Phone phone=new Phone();
			phone.setType("Mobile");	
			phone.setNumber(9005587985L);
			Student student =new Student();
			//student.setRollNo(101);
			student.setName("Princy");
			student.setDept("cse");
			student.setMarks(studentMarks);
			student.setPhoneNo(phone);
			student.setAddress(address);
			session.save(student);
			/*int count=(Integer) session.save(student);
			System.out.println(count);
			Student studentObject=(Student) session.get(Student.class, count);
			Address add=studentObject.getAddress();
			Phone phoneObject=studentObject.getPhoneNo();
			Student_Marks markObject=studentObject.getMarks();*/
			
		//Second record
			Address address1=new Address();
			address1.setStreet("Ashok street");
			address1.setCity("Bangalore");
			address1.setState("Karnataka");
			address.setZipcode(0657467);
			Student_Marks studentMarks1=new Student_Marks();
			studentMarks1.setTamil(30);
			studentMarks1.setEnglish(20);
			studentMarks1.setMaths(43);
			studentMarks1.setScience(40);
			studentMarks1.setSocial(60);
			studentMarks1.setTotal();
			studentMarks1.setResult();
			Phone phone1=new Phone();
			phone1.setType("cell");
			phone1.setNumber(99673628L);
			Student student1 =new Student();
			student1.setName("Raji");
			student1.setDept("IT");
			student1.setPhoneNo(phone1);
			student1.setAddress(address1);
			student1.setMarks(studentMarks1);
			session.save(student1);
			transaction.commit();
		}catch(HibernateException he){
			transaction.rollback();
		}finally{
			session.close();
		}
	}
}
