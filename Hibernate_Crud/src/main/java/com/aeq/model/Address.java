package com.aeq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="STUDENT_ADDRESS")
public class Address {
	@Id
	@Column (name="ADDRESS_ID")
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	@Column (name="STREET")
	private String street;
	@Column (name="CITY")
	private String city;
	@Column (name="STATE")
	private String state;
	@Column (name="ZIPCODE")
	private long zipcode;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public long getZipcode() {
		return zipcode;
	}
	public void setZipcode(long zipcode) {
		this.zipcode = zipcode;
	}
}
