package com.aeq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="PHONE")
public class Phone {
	@Id
	@Column (name="PHONE_ID")
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	@Column (name="PHONE_TYPE")
	private String type;
	@Column (name="PHONE_NUMBER")
	private Long number;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getNumber() {
		return number;
	}
	public void setNumber(Long number) {
		this.number = number;
	}
}
