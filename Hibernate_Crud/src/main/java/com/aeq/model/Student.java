package com.aeq.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table (name="STUDENT")
public class Student {
@Id
@Column (name="STUDENT_ID", unique = true, nullable = false)
@GeneratedValue(strategy=GenerationType.AUTO)
private int rollNo;
@Column (name="STUDENT_NAME")
private String name;
@Column (name="STUDENT_DEPT")
private String dept;
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn (name ="ADDRESS_ID")
private Address address;
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn (name ="MARK_ID")
private Student_Marks marks;
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn (name ="PHONE_ID")
private Phone phoneNo;

public int getRollNo() {
	return rollNo;
}
public void setRollNo(int rollNo) {
	this.rollNo = rollNo;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDept() {
	return dept;
}
public void setDept(String dept) {
	this.dept = dept;
}
public Address getAddress() {
	return address;
}
public void setAddress(Address address) {
	this.address = address;
}
public Student_Marks getMarks() {
	return marks;
}
public void setMarks(Student_Marks marks) {
	this.marks = marks;
}
public Phone getPhoneNo() {
	return phoneNo;
}
public void setPhoneNo(Phone phoneNo) {
	this.phoneNo = phoneNo;
}
}
